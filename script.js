console.log(`Hello`);

 
function createTheTrainer (trainerName, trainerAge, trainerPokemon, trainerFriends){
	this.name = trainerName;
	this.age = trainerAge;
	this.pokemon = trainerPokemon;
	this.friends = trainerFriends;

	this.talk = function(pokemonNo){
		// console.log(this.pokemon); // Call the array
		console.log(`${this.pokemon[pokemonNo]} I choose you!`)
	}
}

function createThePokemon (pokemonName, pokemonLevel, pokemonHealth, pokemonAttack){
	this.pokemonname = pokemonName;
	this.level = pokemonLevel;
	this.health = pokemonHealth;
	this.attack = pokemonAttack;

	this.tackle = function (targetPokemon){
		let tackleDamage = targetPokemon.health - this.attack;
		console.log(tackleDamage);
		if (tackleDamage <= 0){
			console.log(`${targetPokemon.pokemonname} has fainted`);
		} else {
			console.log(`${this.pokemonname} attacked ${targetPokemon.pokemonname} with tackle!`);
			console.log(`${targetPokemon.pokemonname}'s health is reduced to ${tackleDamage}!`)	
		}
	}

	this.faint = function (){
			console.log(`has fainted`);
	}
}


//Create a pokemon
var Pikachu = new createThePokemon("Pikachu", 12, 120, 150);
var Wheezing = new createThePokemon("Wheezing", 25, 150, 18);
var Geodude = new createThePokemon("Geodude", 18, 118, 15);
var Bulbasaur = new createThePokemon("Bulbasaur", 10, 54, 50)

// Create the trainer
var Ash = new createTheTrainer("Ash", 12, [Pikachu, Wheezing, Geodude, Bulbasaur] , [Misty, Brock]);
var Misty = new createTheTrainer("Misty", 15, [Wheezing, Geodude, Pikachu], [Ash, Brock]);
var Brock = new createTheTrainer("Brock", 17, [Geodude, Wheezing], [Ash, Misty]);





//The trainer
console.log(Ash);

//Talk method
console.log(Ash.talk(0));
console.log(Pikachu.tackle(Wheezing));
console.log(Wheezing.tackle(Bulbasaur));